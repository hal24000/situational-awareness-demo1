"""
ProjectCollections object, Contains various mongo collections, data collections and files to be used by the dashboard


Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.

"""
import dimension
import numpy as np
import pandas as pd
import pathlib
import datetime
from bs4 import BeautifulSoup
import pytz
import os

allowed_aggregations = ['mean', 'sum', 'min', 'max']
allowed_moves = [-1, 0, 1]
AVAIL_INSTALLATIONS_OVERRIDE = ['GOODWOOD']
DATETIME_OVERRIDE = datetime.datetime(year=2020, month=3, day=5, hour=18, minute=45)

class ProjectCollections():
    """
    Various variables and settings are defined here and used throughout the page
    """
    tz_london = pytz.timezone('Australia/Adelaide')
    tz_utc = pytz.timezone('utc')

    # VARIOUS DEFAULTS
    ERROR_MSG_TABLE_RGF_GAC = "no data in 60m"
    # PATHS
    PATH = pathlib.Path(__file__).parent
    path_src = pathlib.Path(PATH).parent
    DATA_PATH = PATH.joinpath("data").resolve()

    # connections
    dc_slow = dimension.Connect(os.path.join(path_src, 'conf/mongo_conf_waterdemo.txt'))
    dc_fast = dimension.Connect(os.path.join(path_src, 'conf/mongo_conf_waterdemo.txt'))

    # collections
    # fast queries should be channeled to the "fast" collections

    fast_series_coll = dc_fast.collection('sensor_values')
    fast_alarms_coll = dc_fast.collection('alarm_values')
    fast_events_coll_latest = dc_fast.collection('alarm_occurrences_latest')
    fast_events_coll = dc_fast.collection('alarm_occurrences')
    alarm_severity_coll = dc_fast.collection('alarm_severity_scoring')
    fast_pump_duty_coll = dc_fast.collection('pump_daily_run_status')
    fast_speed_events_coll = dc_fast.collection('speed_occurrence_events')

    series_coll = dc_slow.collection('sensor_values')
    alarms_coll = dc_slow.collection('alarm_values')
    site_tag_config_coll = dc_slow.collection(('site_tag_config'))
    events_coll_latest = dc_slow.collection('alarm_occurrences_latest')
    events_coll = dc_slow.collection('alarm_occurrences')
    pump_duty_coll = dc_slow.collection('pump_daily_run_status')
    maintenance_coll = dc_slow.collection('maintenance_workorders')
    speed_events_coll = dc_slow.collection('speed_occurrence_events')

    # local dataframes
    installation_df = dc_slow.get_collection('installations')
    installation_df.drop("_id", axis=1, inplace=True)

    site_tag_config = dc_slow.get_collection('site_tag_config').drop('_id', axis=1)
    site_tag_config['tag_description'].fillna("", inplace=True)

    # wq tags as described in tracebility matrix under RiskScoringWaterQuality
    wq_tags_desc_dict = {
            'GOODW': ['Raw Water pH',
                      'Clarified Water Turbidity',
                      'Dosed Raw Water pH (TVAL)',
                      'Comb RGF Filtered Water UV254',
                      'RGF Filtered Water Turbidity',
                      'Post Bisul Cl Level (TVAL)',
                      'Cont Tank Inlet Free Cl (mg/l)',
                      'Comb GAC Filtered Turbidity',
                      'Pre GAC pH (TVAL)',
                      'Treat Water Alum µg/l',
                      'Treat Water Cl (Total) (mg/l)',
                      'Treat Water Final pH',
                      'Treated Water Turbidity',
                      ]
    }
    wq_tag_names_dict = {}
    wq_tag_name_detail_dict = {}

    # load metric config
    query = {'direction_of_change': {'$ne': None}}
    metric_config = pd.DataFrame(list(site_tag_config_coll.find(query, {'_id': 0})))
    metric_config = metric_config.query("tag_category != 'Decommision'")

    metric_config.rename(columns={'tag_short_description': 'metric',
                                  'site_name':             'location'},
                         inplace=True)

    # filter out unknown move values
    metric_config = metric_config[metric_config['direction_of_change'].map(lambda x: x in allowed_moves)]

    # filter out unknown aggregations
    metric_config['risk_scoring_aggregation'] = metric_config['risk_scoring_aggregation'].map(lambda x: x.lower())
    metric_config = metric_config[metric_config['risk_scoring_aggregation'].map(lambda x: x in allowed_aggregations)]

    # filter out zero thresholds for ROC
    metric_config = metric_config[metric_config['threshold_rate_of_change'] > 0]

    # assign cutoffs
    metric_config['cutoff_drop'] = metric_config.apply(
            lambda row: -row['threshold_rate_of_change'] if row['direction_of_change'] in [-1, 0] else -np.inf, axis=1
    )

    metric_config['cutoff_rise'] = metric_config.apply(
            lambda row: row['threshold_rate_of_change'] if row['direction_of_change'] in [0, 1] else np.inf, axis=1
    )

    metric_config['risk_scoring_period'] = metric_config['risk_scoring_period'].astype(int)

    ### get stock level tags
    # 1. get only tags that have chemical subcategory
    # 2. for each asset get the tags that have an absolute value UOM, if not avail get %

    def custom_agg(series):
        'sort values by measurement unit, return non % stuff first'
        series_sorted = series.sort_values(by='measurement_unit')
        series_ret = series_sorted.iloc[-1, :]
        return series_ret

    stock_level_entries = site_tag_config.loc[(site_tag_config.tag_category == 'StockLevel') &
                                              (site_tag_config.tag_subcategory.str.lower() == 'chemical') &
                                              (site_tag_config.measurement_unit != ' Text')]

    stock_level_tags_df = stock_level_entries[['tag_short_description',
                                               'tag_name',
                                               'site_name',
                                               'functional_area',
                                               'measurement_unit']].groupby('tag_short_description').agg(custom_agg)

    wq_tags_df = site_tag_config.loc[site_tag_config.tag_category.str.lower() == 'wq']

    gac_tags = list(site_tag_config.loc[site_tag_config.process_area.fillna("").str.lower().str.contains(
            'gac'), 'tag_name'].unique())

    # bearing / motor OT tags
    bearing_ot_tags_df = site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains('BOT') |
                                             site_tag_config.tag_subcategory.str.upper().str.contains("TEMPBOT")]
    motor_ot_tags_df = site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains('MOT') |
                                           site_tag_config.tag_subcategory.str.upper().str.contains("TEMPMOT")]

    avail_installations = [inst for inst in installation_df.site_name.unique() if inst in AVAIL_INSTALLATIONS_OVERRIDE]

    schematics_dict_bs = {}

    for installation_ in avail_installations:
        try:
            string_svg = open(os.path.join(path_src, 'visualization/assets/images/{}_FLOW.svg'.format(installation_)),
                              'r').read()
        except:
            string_svg = open(os.path.join(path_src, 'visualization/assets/images/{}.svg'.format(installation_)),
                              'r').read()
        schematics_dict_bs[installation_] = BeautifulSoup(string_svg, features='lxml-xml')

    # TODO: These configs should live ELSEWHERE
    # date formatting
    dashboard_date_format = '%d/%m/%Y'
    dashboard_time_format = '%H:%M'
    dashboard_datetime_format = '{} {}'.format(dashboard_date_format, dashboard_time_format)

    # pick which loading animation to use thorughout the dashboard
    loading_animation = 'default'

    # flow tags for the schematics
    # returns more tags but not too many more
    flow_tag_df = site_tag_config.loc[(site_tag_config['tag_category'] == 'Flow')
                                      & (site_tag_config['tag_subcategory'] == 'Site Throughput')
                                      & (site_tag_config['measurement_unit'] != 'Text')].copy()

    # add multiplicative factors where needed
    flow_tag_df['mul_fact'] = 1.
    flow_tag_df.loc[flow_tag_df['tag_name'] == 'HUBYWTS1:COMBINED_RGF_FLOW_RAW',
                    'mul_fact'] = 0.018

    # define mapping of the work units
    work_amount_unit_map = {'minute': 'T'}

    @classmethod
    def get_mongo_collection(cls, collection, lookback):
        if collection not in ['sensors', 'alarms', 'events']:
            raise ValueError('expected sensors or alarms')

        if lookback > 25:
            if collection == 'sensors':
                return cls.series_coll
            elif collection == 'alarms':
                return cls.alarms_coll
            elif collection == 'events':
                return cls.events_coll
        else:
            if collection == 'sensors':
                return cls.fast_series_coll
            elif collection == 'alarms':
                return cls.fast_alarms_coll
            elif collection == "events":
                return cls.fast_events_coll
